extern crate bindgen;
extern crate cc;

use std::env;
use std::path::PathBuf;

const LIBC_TYPES: &[&'static str] = &[
    "sockaddr",
    "sockaddr_in",
    "sockaddr_in6",
    "in_addr",
    "in6_addr",
    "timespec",
];

const USELESS_TYPES: &[&'static str] = &["in_addr_t", "in_port_t", "in6_addr__bindgen_ty_1"];

fn build_bindings() {
    let mut bindings = bindgen::Builder::default()
        .header("c/wireguard.h")
        .impl_debug(true)
        .whitelist_function("wg_.*")
        .bitfield_enum("wg_peer_flags")
        .bitfield_enum("wg_device_flags")
        .raw_line("extern crate libc;");

    for ty in LIBC_TYPES {
        bindings = bindings
            .blacklist_type(ty)
            .raw_line(format!("use libc::{};", ty));
    }

    for ty in USELESS_TYPES {
        bindings = bindings.blacklist_type(ty);
    }

    let bindings = bindings.generate().expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}

fn build_library() {
    cc::Build::new()
        .file("c/wireguard.c")
        .warnings(true)
        .extra_warnings(true)
        .warnings_into_errors(true)
        .flag_if_supported("-Wno-unused-parameter")
        .compile("wireguard");
}

fn main() {
    build_bindings();
    build_library();
}
