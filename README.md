# wgctrl-rs
This is really hacky, pre-pre-pre-alpha quality software. Nothing works, and things that do work don't actually work. Hide your cat.

[Docs](https://k900.gitlab.io/wgctrl-rs/wgctrl_rs/index.html) (wip!)

# License
LGPL 2.1, cause that's what the wireguard C library is licensed under

# What works
* key generation
* key -> base64 and base64 -> key conversion
* reading iface configuration
* writing iface configuration (incl. partial updates, similar to CLI wg tool)

# TODO
* clean up API
* implement setconf/showconf/addconf?
* better error handling
* more localized unsafeness
* fix potential UB wrt Key constness

# Misc notes
* this is intended to be really high level, so no linked lists, rewriting everything all the time, lots of overhead, explicit peer add/remove operations and such
* non-Linux (wireguard-go, wireguard-rs, etc) will require a whole other backend
* the name is not final because wireguard-rs exists - maybe wgapi-rs or something?
