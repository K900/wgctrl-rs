extern crate wgctrl_rs;

use wgctrl_rs::*;

fn main() {
    for dev in DeviceInfo::enumerate().expect("Failed to enumerate devices!") {
        println!(
            "{:#?}",
            DeviceInfo::get_by_name(&dev).expect("Failed to get device!")
        );
    }
}
