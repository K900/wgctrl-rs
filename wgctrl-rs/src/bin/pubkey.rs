extern crate wgctrl_rs;

use wgctrl_rs::*;

use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        println!(
            "{}",
            Key::from_base64(&line.expect("Failed to read line!"))
                .expect("Failed to parse key!")
                .generate_public()
                .to_base64()
        );
    }
}
